class CreateGames < ActiveRecord::Migration[5.0]
  def change
    create_table :games do |t|
      t.string :name
      t.string :word
      t.timestamps
    end
    
    create_table :letters do |t|
      t.belongs_to :game, index: true
      t.string :value
      t.timestamps
    end
  end
end
