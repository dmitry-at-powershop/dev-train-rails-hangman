Rails.application.routes.draw do
  root 'games#index'
  resources :games, only: [:new, :index, :show, :create, :destroy] do
    resources :letters, only: [:index, :create]
  end
end
