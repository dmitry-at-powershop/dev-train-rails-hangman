require 'rails_helper'

require 'simplecov'
SimpleCov.start

RSpec.describe Game, type: :model do
  describe '.initialize' do
    context 'given a name and a word' do
      let(:game) { Game.new('name' => 'superhangman', 'word' => 'superword') }
      it 'sets the game with the name and the word' do
        expect(game.name).to eql('superhangman')
        expect(game.word).to eql('superword')
      end
    end
    context 'given nothing' do
      let(:game) { Game.new }
      it 'sets the game with no name and a random word' do
        expect(game.name).to match(/\AGame \d*\z/)
        expect(game.word).to match(/\A[a-z]+\z/)
      end
    end
    context 'given a bad word' do
      it 'raises an error on non lowercase letters' do
        expect(Game.new('word' => 'xxxAxx').valid?).to be_falsey
      end
    end
  end

  describe '.playing?' do
    let(:game) { Game.new(word: 'xyz') }

    context 'with no letters guessed and initial number of attempts' do
      it 'returns false' do
        expect(game.playing?).to eql(true)
      end
    end

    context 'with some letters guessed and positive number of attempts' do
      before do
        game.save
        %w(a b x).map{ |x| Letter.create(game_id: game.id, value: x) }
      end
      it 'returns false' do
        expect(game.playing?).to eql(true)
      end
    end

    context 'with all letters guessed (and some attempts left)' do
      before do
        game.save
        %w(a b x y z).map{ |x| Letter.create(game_id: game.id, value: x) }
      end
      it 'returns false' do
        expect(game.playing?).to eql(false)
      end
    end

    context 'with some letters guessed and 0 attempts left' do
      before do
        game.save
        %w(a b c d e f x z).map{ |x| Letter.create(game_id: game.id, value: x) }
      end
      it 'returns false' do
        expect(game.playing?).to eql(false)
      end
    end
  end



end
