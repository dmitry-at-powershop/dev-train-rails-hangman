require 'rails_helper'

RSpec.describe GamesController, :type => :controller do

  describe "GET #index" do
    it "responds successfully with an HTTP 200 status code" do
      get :index
      expect(response).to be_success
      expect(response).to have_http_status(200)
    end

    it "renders the index template" do
      get :index
      expect(response).to render_template("index")
    end

    it "loads all of the games into @games" do
      game1, game2 = Game.create!, Game.create!
      get :index

      expect(assigns(:games)).to match_array([game1, game2])
    end
  end

  describe "GET #show" do
    it "renders the show template" do
      game = Game.create!
      get :show, params: { id: game.to_param }

      expect(response).to render_template("show")
    end

    it "loads the requested game as @game" do
      game = Game.create!
      get :show, params: { id: game.to_param }

      expect(assigns(:game)).to eq(game)
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested game" do
      game = Game.create!
      expect { delete :destroy, params: { id: game.to_param } }.to change(Game, :count).by(-1)
    end

    it "redirects to the games list" do
      game = Game.create!
      delete :destroy, params: { id: game.to_param }
      expect(response).to redirect_to(games_path)
    end
  end

  describe "POST #create" do
    context "with default params" do
      let (:game_params) {{ name: 'test', word: 'powershop' }}

      it "creates a new Game" do
        expect { post :create, params: { game: game_params } }.to change(Game, :count).by(1)
      end

      it "puts the new game to @game" do
        post :create, params: { game: game_params }
        expect(assigns(:game)).to be_a(Game)
        expect(assigns(:game)).to be_persisted
      end

      it "redirects to the new game" do
        post :create, params: { game: game_params }
        expect(response).to redirect_to(game_path(Game.last.id))
      end
    end
  end
end
