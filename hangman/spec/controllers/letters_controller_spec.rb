require 'rails_helper'

RSpec.describe LettersController, type: :controller do
  describe "POST #create" do
    context "with a new letter" do
      it "adds the letter to the game" do
        game = Game.create
        post :create, params: { game_id: game.id, letter: 'a' }
        expect(assigns(:letter)).to be_a(Letter)
        expect(assigns(:letter)).to be_persisted
      end
    end
  end
end
