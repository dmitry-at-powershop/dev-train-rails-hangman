module GamesHelper
  def secret_word(game)
    game.playing? ? game.masked_word.gsub(/\0/, "\u200A_\u200A") : game.word.upcase
  end

  def failed_letters(game)
    game.failed_letters.join(', ').upcase
  end

end
