class Game < ApplicationRecord
  MASK_CHAR = "\0".freeze
  MAX_FAILS = 6

  has_many :letters, dependent: :destroy

  validates_format_of :word, with: /\A[a-z]+\z/
  # validate my_custom_stuff

  after_initialize do
    self.word = random_word if word.blank?
  end

  def name
    return "Game #{id}" if read_attribute(:name).blank?
    read_attribute(:name)
  end

  def masked_word
    tried_letters = self.tried_letters
    word.chars.reduce('') do |masked, letter|
      masked += tried_letters.include?(letter) ? letter.upcase : MASK_CHAR
    end
  end

  def status
    result || 'Playing'
  end

  def playing?
    !result
  end

  def failed_letters
    tried_letters.select{ |letter| ! word.include? letter }
  end

  def tried_letters
      letters.all.map{ |element| element.value }
  end

  def result
    return 'Won' unless masked_word.include? MASK_CHAR
    return 'Lost' if failed_letters.length >= MAX_FAILS
  end

  private

  def random_word
    File.readlines('app/models/words.txt').sample.chomp
  end
end
