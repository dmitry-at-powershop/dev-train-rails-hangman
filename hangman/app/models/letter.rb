class Letter < ApplicationRecord
  belongs_to :game
  validates_length_of :value, is: 1
  validates_format_of :value, with: /\A[a-z]\z/
  validates_uniqueness_of :value, scope: :game_id
end
