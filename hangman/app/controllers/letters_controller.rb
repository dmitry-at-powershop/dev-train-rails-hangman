class LettersController < ApplicationController
  def create
    @game = Game.find(params[:game_id])
    @letter = @game.letters.create(value: params[:letter])
    @letter.save
    redirect_to game_path(@game)
  end
end
