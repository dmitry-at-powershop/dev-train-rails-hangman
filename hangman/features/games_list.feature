Feature: Games List
  In order to manage games
  As a player
  I want create, play and remove games

  Scenario: Viewing the games list
    Given some games exist
    When I am on the games list page
    Then I can view the list of games

  Scenario: Creating a game
    When I am on the games list page
    Then I can start a new game

  Scenario: Viewing a game
    Given finished games exist
    When I am on the games list page
    Then I can view finished games

  Scenario: Playing a game
    Given games in progress exist
    When I am on the games list page
    Then I can play a game in progress
