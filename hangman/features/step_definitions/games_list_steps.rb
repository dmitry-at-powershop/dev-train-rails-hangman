Given(/^some games exist$/) do
  Game.create!(name: 'Testgame1', word: 'powershop')
  Game.create!(name: 'Testgame2', word: 'powershop')
end

When(/^I am on the games list page$/) do
  visit games_path
end

Then(/^I can view the list of games$/) do
  expect(page).to have_content('Testgame1')
  expect(page).to have_content('Testgame2')
end

Then(/^I can start a new game$/) do
  click_link 'New game'
  fill_in('Name (you may leave it empty)', :with => 'Capybara\' game')
  fill_in('Word (you may leave it empty)', :with => 'capybara')
  click_button('Start Game')

  expect(current_path).to match(%r{/games/})
  expect(page).to have_content('Playing Capybara\' game')
  expect(page).to have_content('_  _  _  _  _  _  _  _')
end

Given(/^finished games exist$/) do
  game = Game.create!(name: 'Testgame3', word: 'xyz')
  Letter.create(game_id: game.id, value: 'x')
  Letter.create(game_id: game.id, value: 'y')
  Letter.create(game_id: game.id, value: 'z')
end

Then(/^I can view finished games$/) do
  expect(page).to have_link("Show")
  click_link('Show')
  expect(page).to have_content(/Won|Lost/)
end

Given(/^games in progress exist$/) do
  game = Game.create!(name: 'Testgame4', word: 'xyz')
  Letter.create(game_id: game.id, value: 'x')
end

Then(/^I can play a game in progress$/) do
  expect(page).to have_link("Play")
  click_link('Play')
  expect(page).to have_content('Playing')
  expect(page).to have_link("A", :href => game_letters_path(Game.last, 'letter' => 'a'))
end
